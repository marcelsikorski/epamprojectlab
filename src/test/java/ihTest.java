import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ihTest {
    @Test
    void twoPlusTwoShouldEqualFour(){
        var calculator = new ih();
        assertEquals(4, calculator.add(2,2));
    }

    @Test
    void threePlusTwoShouldEqualFour(){
        var calculator = new ih();
        assertThrows(IllegalArgumentException.class, () ->{
            calculator.add(4,7);
        });
        //assertTrue(calculator.add(3,7) == 10);
    }
}